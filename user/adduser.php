<?php
include(dirname(__FILE__).'/isboleh.php');
?>

<div class="container" style="margin-top:90px">
	<div class="row">
		<?php include('./static/nav-left.php'); ?>
		<div class="col-md-10">
			<div class="panel panel-success">
				<!-- Default panel contents -->
					<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <span class="glyphicon glyphicon-chevron-right"></span> Tambah Pengguna</div>
						
			<div class="panel-body" >
			
				<form  role="form" method="post" action="user/isiuser.php" AUTOCOMPLETE="off" >
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label" for="username" >Username : </label>
							<div class="input-group col-md-4" >
							
								<span class="input-group-addon" ><span class="glyphicon glyphicon-user"></span></span>
								<input name="username" type="text" class="form-control "   id="username" placeholder="Username">
							
							</div>
						
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">Password : </label>
						<div class="input-group col-md-4">
						
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input name="pass" type="password" class="form-control"   id="pass" placeholder="Password">
						
						</div>
						<div class="col-md-4 " ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">Ketik Kembali Password : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input name="pass2" type="password" class="form-control"   id="pass2" placeholder="Ketik Kembali Password">
						</div>
						<div class=" col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">Nama Lengkap : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-font"></span></span>
							<input name="fullname" type="text" class="form-control"   id="fullname" placeholder="Nama Lengkap">
						</div>
						<div class="col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">Title : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-tower"></span></span>
							<input name="titel" type="text" class="form-control"   id="titel" placeholder="Title">
						</div>
						<div class="col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">E-mail : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
							<input name="email" type="email" class="form-control"   id="email" placeholder="E-mail">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label " for="alamat">Alamat : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
							<input name="alamat" type="text" class="form-control"   id="alamat" placeholder="Alamat">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_1">No. Telepon 1 : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp1" type="text" class="form-control"   id="telp_1" placeholder="No. Telepon 1">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_2">No. Telepon 2 : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp2" type="text" class="form-control"   id="telp_2" placeholder="No. Telepon 2">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_3">No. Telepon 3 : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp3" type="text" class="form-control"   id="telp_3" placeholder="No. Telepon 3">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_kantor">No. Telepon Kantor : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp_kantor" type="text" class="form-control"   id="telp_kantor" placeholder="No. Telepon Kantor">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="grupid">Jabatan : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-wrench"></span></span>
						<select name="grupid"  class="form-control"   id="grupid">
								<?php 
								if ($_SESSION['grupid']=='SA')
								echo "<option value=";
								?>
								"SA"
								<?php
								echo ">SA - System Admin</option>";
								?>
								<!--<option value="ADM">ADM - Administrator</option>-->
								<option value="BDH">BDH - Bendahara</option>
								<!--<option value="MGT">MGT - Management</option>-->
								<option value="PIP">PIP - PIC Project</option>
								<option value="TEM">TEM - Team Member</option>
								
							</select>
						
						</div>
						</div>
						<div class="input-group col-md-4" ></div>
						<div class="row" >
						<div class="col-md-2 " ></div>
					<div class="col-md-4 " ><button type="submit" class="btn btn-success btn-block">Tambahkan</button></div>
					<div class="col-md-4 " ><button type="reset" onClick="history.go(0)" class="btn btn-danger btn-block">Reset</button></div>
						<div class="col-md-2 " ></div>
					
					</div>
					</div>
					
					
					
				</form>
				
				
			</div>
						
						
</div>
	</div>
	<?php include('./static/pre-footer.php'); ?>
</div>