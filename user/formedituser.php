<?php
include(dirname(__FILE__).'/isboleh.php');
?>
<?php
include('./mysql_con.php');
$id=$_GET['id'];
$query='SELECT * FROM user WHERE user_id='.$id;
$result=mysql_query($query);
$data=mysql_fetch_array($result);
?>
<script src="./js/jquery-1.10.2.js">
</script>
<script >

$(document).ready(function(){

    $(".cepa").hide();
  $("#cek").click(function(){

    $(".cepa").toggle();
  });
});
</script>



<div class="container" style="margin-top:90px">
	<div class="row">
		<?php include('./static/nav-left.php'); ?>
		<div class="col-md-10">
			<div class="panel panel-success">
				<!-- Default panel contents -->
					<div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <span class="glyphicon glyphicon-chevron-right"></span> Ubah Data Pengguna</div>
						
			<div class="panel-body" >
			
				<form  role="form" method="post" action="user/edituser.php" AUTOCOMPLETE="off" >
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label" ></label>
						<div class="input-group col-md-4 " >
							<span class="input-group-addon" >#</span>
							<input name="id" type="text" class="form-control "   readonly value="<?php echo $data['user_id'];?>" placeholder="id">
						</div>
				
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label" for="username" >Username : </label>
							<div class="input-group col-md-4" >
							
								<span class="input-group-addon" ><span class="glyphicon glyphicon-user"></span></span>
								<input name="username" type="text" class="form-control " readonly value="<?php echo $data['username'];?>"  id="username" placeholder="Username">
							
							</div>
						
					</div>
					
					<div class="form-group">
						<div class="col-md-4"></div>
						<div class=" input-group col-md-4" > 
							<div class="alert alert-info">
							<input id="cek"  type="checkbox" name="changepass" value="Yes">
							<label class=" control-label" for="cek">Ubah Password ?</label>
							</div>
						</div>
			
					</div>
					
					<div class="form-group cepa">
						<label class="col-md-4 text-right control-label">Password Lama : </label>
						<div class="input-group col-md-4">
						
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input name="passold" type="password" class="form-control"   id="passold" placeholder="Password Lama">
						
						</div>
						
					</div>
					
					<div class="form-group cepa">
						<label class="col-md-4 text-right control-label">Password Baru : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input name="pass1" type="password" class="form-control"   id="pass1" placeholder="Password Baru">
						</div>
						
					</div>
					
					<div class="form-group cepa">
						<label class="col-md-4 text-right control-label">Ketik Kembali Password Baru : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input name="pass2" type="password" class="form-control"   id="pass2" placeholder="Ketik Kembali Password Baru">
						</div>
						
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">Nama Lengkap : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-font"></span></span>
							<input name="fullname" type="text" class="form-control" value="<?php echo $data['fullname'];?>"  id="fullname" placeholder="Nama Lengkap">
						</div>
						
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">Title : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-tower"></span></span>
							<input name="titel" type="text" class="form-control" value="<?php echo $data['title'];?>"  id="titel" placeholder="Title">
						</div>
						<div class="col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label">E-mail : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-envelope"></span></span>
							<input name="email" type="email" class="form-control"   value="<?php echo $data['email'];?>" id="email" placeholder="E-mail">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 text-right control-label " for="alamat">Alamat : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-bookmark"></span></span>
							<input name="alamat" type="text" class="form-control" value="<?php echo $data['alamat'];?>"  id="alamat" placeholder="Alamat">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_1">No. Telepon 1 : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp1" type="text" class="form-control" value="<?php echo $data['telp1'];?>"   id="telp_1" placeholder="No. Telepon 1">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_2">No. Telepon 2 : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp2" type="text" class="form-control" value="<?php echo $data['telp2'];?>"  id="telp_2" placeholder="No. Telepon 2">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_3">No. Telepon 3 : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp3" type="text" class="form-control" value="<?php echo $data['telp3'];?>"  id="telp_3" placeholder="No. Telepon 3">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
					<div class="form-group">
						<label class="col-md-4 control-label text-right" for="telp_kantor">No. Telepon Kantor : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-phone-alt"></span></span>
							<input name="telp_kantor" type="text" class="form-control"  value="<?php echo $data['telp_kantor'];?>"  id="telp_kantor" placeholder="No. Telepon Kantor">
						</div>
						<div class="input-group col-md-4" ></div>
					</div>
					
				<div class="form-group <?php if(!isboleh()){ ?>hidden<?php } ?>" >
						<label class="col-md-4 control-label text-right" for="grupid">Jabatan : </label>
						<div class="input-group col-md-4">
							<span class="input-group-addon"><span class="glyphicon glyphicon-wrench"></span></span>
			<select  name="grupid" class="form-control">
				<option value="SA" <?php if ($data['grupid']=='SA'){echo "selected";}?>>SA - System Admin</option>
				<option value="BDH" <?php if ($data['grupid']=='BDH'){echo "selected";}?>>BDH - Bendahara</option>
				<option value="MGT" <?php if ($data['grupid']=='MGT'){echo "selected";}?>>MGT - Management</option>
				<option value="PIP" <?php if ($data['grupid']=='PIP'){echo "selected";}?>>PIP - PIC Project</option>
				<option value="TEM" <?php if ($data['grupid']=='TEM'){echo "selected";}?>>TEM - Team Member</option>
				
				</select>
						
						</div>
						</div>
						
				
					
					
					
					
						<div class="input-group col-md-4" ></div>
						<div class="row" >
						<div class="col-md-2 " ></div>
					<div class="col-md-4 " ><button type="submit" class="btn btn-warning btn-block">Ubah</button></div>
					<div class="col-md-4 " ><button type="reset" onClick="history.go(0)" class="btn btn-success btn-block">Reset</button></div>
						<div class="col-md-2 " ></div>
					
					</div>
					</div>
					
					
					
				</form>
				
				
			</div>
						
						
</div>
	</div>
	<?php include('./static/pre-footer.php'); ?>
</div>