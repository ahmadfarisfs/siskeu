<?php
include(dirname(__FILE__).'/../mysql_con.php');

$query=" CREATE TABLE IF NOT EXISTS `log_transaksi` (
  `transaksiID` int(11) NOT NULL,
  `bukti` varchar(100) NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `jenis` varchar(10) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `tipe` varchar(10) NOT NULL,
  `tanggal` date NOT NULL,
  `datafrom` datetime NOT NULL,
  `historyID` int(11) NOT NULL AUTO_INCREMENT,
  `proyekID` int(11) NOT NULL,
  `createdBy` varchar(50) NOT NULL,
  PRIMARY KEY (`historyID`)
)";
$result=mysql_query($query);
$query2="CREATE TABLE IF NOT EXISTS `project` (
  `proyekID` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `lokasi` varchar(50) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `deskripsi` varchar(200) NOT NULL,
  `tanggal` date NOT NULL,
  `status` varchar(10) NOT NULL,
  `saldo` bigint(20) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL,
  `PICID` varchar(10) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `changedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `createdBy` varchar(50) NOT NULL,
  PRIMARY KEY (`proyekID`)
)";
$result2=mysql_query($query2);

$query3="CREATE TABLE IF NOT EXISTS `team_member` (
  `temID` int(11) NOT NULL AUTO_INCREMENT,
  `proyekID` int(11) NOT NULL,
  `member` varchar(50) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`temID`)
)";
$result3=mysql_query($query3);

$query4="CREATE TABLE IF NOT EXISTS `transaksi` (
  `transaksiID` int(11) NOT NULL AUTO_INCREMENT,
  `proyekID` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `deskripsi` varchar(100) NOT NULL,
  `tipe` varchar(15) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `bukti` varchar(50) NOT NULL,
  `approval` varchar(5) NOT NULL,
  `saldoawal` bigint(20) NOT NULL,
  `saldoakhir` bigint(20) NOT NULL,
  `createdOn` datetime NOT NULL,
  `createdBy` varchar(50) NOT NULL,
  `changedOn` datetime NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `jenis` varchar(3) NOT NULL,
  `deletedOn` datetime NOT NULL,
  PRIMARY KEY (`transaksiID`)
)";
$result4=mysql_query($query4);

$query5="CREATE TABLE IF NOT EXISTS `transfer` (
  `transferID` int(11) NOT NULL AUTO_INCREMENT,
  `changedOn` datetime NOT NULL,
  `dstacc` varchar(50) NOT NULL,
  `srcprj` int(11) NOT NULL,
  `dstaprj` int(11) NOT NULL,
  `nilai` bigint(20) NOT NULL,
  `tanggal` date NOT NULL,
  `approval` int(11) NOT NULL,
  `isDeleted` int(11) NOT NULL,
  `bukti` varchar(50) NOT NULL,
  `deskripsi` varchar(50) NOT NULL,
  `createdOn` datetime NOT NULL,
  `createdBy` varchar(50) NOT NULL,
  `approvedOn` datetime NOT NULL,
  `tanggallama` date NOT NULL,
  `deskripsilama` varchar(100) NOT NULL,
  `nilailama` bigint(20) NOT NULL,
  `buktilama` varchar(100) NOT NULL,
  `transaksiIDdst` int(11) NOT NULL,
  `transaksiIDsrc` int(11) NOT NULL,
  PRIMARY KEY (`transferID`)
)";
$result5=mysql_query($query5);

$query6="CREATE TABLE IF NOT EXISTS `user` (
  `user_id` int(4) NOT NULL AUTO_INCREMENT,
  `username` varchar(10) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `telp1` varchar(50) NOT NULL,
  `telp2` varchar(50) NOT NULL,
  `telp3` varchar(50) NOT NULL,
  `telp_kantor` varchar(50) NOT NULL,
  `grupid` varchar(3) NOT NULL,
  `createdOn` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `isDeleted` tinyint(1) NOT NULL,
  `changedOn` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`user_id`),
  KEY `username` (`username`)
)";
$result6=mysql_query($query6);

$query7="INSERT INTO `user` (`user_id`, `username`, `password`, `email`, `fullname`, `title`, `alamat`, `telp1`, `telp2`, `telp3`, `telp_kantor`, `grupid`, `createdOn`, `isDeleted`, `changedOn`) VALUES
(1, 'SA', '3dd6b9265ff18f31dc30df59304b0ca7', '', '', '', '', '', '', '', '', 'SA', '2014-06-15 21:36:23', 0, '2014-06-16 16:42:44')
";
$result7=mysql_query($query7);


if($result && $result2 && $result3 && $result4 && $result5 && $result6 && $result7){
 // header("Location: ../main.php?kon=viewproject"); 
  echo "Instalasi Berhasil ! - Isi Data";
}else{
    die('Insatalasi Gagal '.mysql_error()); //jika gagal tampilkan errornya
}
?>