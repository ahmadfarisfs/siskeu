<?php
include(dirname(__FILE__).'/isboleh.php');
?>
<?php
include('./mysql_con.php');
$id=$_GET['id'];

$query='SELECT * FROM transaksi WHERE transaksiID='.$id;

$result=mysql_query($query);
$data=mysql_fetch_array($result);
?>


<script>
	$(function() {
$('.form-group.date').datepicker({
    format: "yyyy-mm-dd",
    todayBtn: "linked",
    keyboardNavigation: false,
    forceParse: false,
    autoclose: true,
    todayHighlight: true
});
	$('#nilai').priceFormat({
      prefix: 'Rp ',
     
      centsLimit: 0,
	  thousandsSeparator: '.'
       });

  });
  
</script>

<div class="container" style="margin-top:90px">
	<div class="row">
		<?php include('./static/nav-left.php'); ?>
		<div class="col-md-10">
			<div class="panel panel-success">
				<!-- Default panel contents -->
					<div class="panel-heading"><span class="glyphicon glyphicon-briefcase"></span> <span class="glyphicon glyphicon-chevron-right"></span> Ubah Transaksi</div>
						
			<div class="panel-body" >
			<form  role="form" method="post" action="transaction/edittransaction.php" AUTOCOMPLETE="off" >
				
				<div class="form-group">
									<label class="col-md-4 text-right control-label" for="proyek" >Proyek : </label>
									<div class="input-group col-md-4" >		
									<span class="input-group-addon" ><span class="glyphicon glyphicon-road"></span></span>
									<input readonly value="<?php echo $data['proyekID'];?>" name="proyek" type="text" class="form-control "   id="proyek" placeholder="proyek">
									</div>
				</div>
				<div class="form-group">
									<label class="col-md-4 text-right control-label" for="transaksiid" >ID Transaksi : </label>
									<div class="input-group col-md-4" >		
									<span class="input-group-addon" ><span class="glyphicon glyphicon-briefcase"></span></span>
									<input readonly value="<?php echo $data['transaksiID'];?>" name="transaksiid" type="text" class="form-control "   id="transaksiid" placeholder="ID">
									</div>
				</div>

				<div class="form-group date">
									<label class="col-md-4 text-right control-label" for="datepicker" >Tanggal : </label>
									<div class="input-group col-md-4" >		
									<span class="input-group-addon" ><span class="glyphicon glyphicon-calendar"></span></span>
									<input value="<?php echo $data['tanggal'];?>" name="tanggal" type="text" class="form-control "   id="" placeholder="Tanggal">
									</div>
				</div>
				<div class="form-group">
									<label class="col-md-4 text-right control-label" for="lokasi" >Deskripsi : </label>
									<div class="input-group col-md-4" >		
									<span class="input-group-addon" ><span class="glyphicon glyphicon-tasks"></span></span>
									<input value="<?php echo $data['deskripsi'];?>" name="deskripsi" type="text" class="form-control "   id="deskripsi" placeholder="Deskripsi">
									</div>
				</div>
				<div class="form-group">
									<label class="col-md-4 text-right control-label" for="tipe" >Tipe : </label>
									<div class="input-group col-md-4" >		
										<div class="radio-inline">
										  <label>
										    <input type="radio" name="tipe" id="tipe1" value="DEB" <?php if($data['tipe']=="DEB"){echo " checked ";}; ?> >
										    Debet
										  </label>
										</div>
										<div class="radio-inline">
										  <label>
										    <input type="radio" name="tipe" id="tipe2" value="KRE" <?php if($data['tipe']=="KRE"){echo " checked ";}; ?>>
										    Kredit
										    </label>
										</div>
									</div>
				</div>
				<div class="form-group">
									<label class="col-md-4 text-right control-label" for="jenis" >Jenis : </label>
									<div class="input-group col-md-4" >		
									<span class="input-group-addon" ><span class="glyphicon glyphicon-shopping-cart"></span></span>
									<select name="jenis" class="form-control" id="jenis">
												
										<option <?php if($data['jenis']=="REN"){echo " selected ";}; ?>value="REN">Renumerasi</option>
										<option <?php if($data['jenis']=="AKO"){echo " selected ";}; ?>value="AKO">Akomodasi</option>
										<option <?php if($data['jenis']=="TRA"){echo " selected ";}; ?>value="TRA">Transportasi</option>
										<option <?php if($data['jenis']=="ATK"){echo " selected ";}; ?>value="ATK">ATK Komunikasi</option>
										<option <?php if($data['jenis']=="KOM"){echo " selected ";}; ?>value="KOM">Komputasi</option>
										<option <?php if($data['jenis']=="MEC"){echo " selected ";}; ?> value="MEC">Mechanical</option>
										<option <?php if($data['jenis']=="CON"){echo " selected ";}; ?>value="CON">Control Electronic</option>
										<option <?php if($data['jenis']=="TRF"){echo " selected ";}; ?>value="TRF">Transfer</option>
									
									</select>
									</div>
				</div>	
				<div class="form-group">
									<label class="col-md-4 text-right control-label" for="nilai" >Nilai : </label>
									<div class="input-group col-md-4" >		
									<span class="input-group-addon" >Rp</span>
									<input value="<?php echo $data['nilai'];?>" name="nilai" type="text" class="form-control " defaultValue=0  id="nilai" placeholder="Nilai">
									</div>
				</div>
				<div class="form-group">
									<label class="col-md-4 text-right control-label" for="bukti" >Bukti : </label>
									<div class="input-group col-md-4" >		
									<span class="input-group-addon" ><span class="glyphicon glyphicon-tasks"></span></span>
									<input value="<?php echo $data['bukti'];?>" name="bukti" type="text" class="form-control "   id="bukti" placeholder="Bukti">
									</div>
				</div>
						<div class="row" >
						<div class="col-md-2 " ></div>
					<div class="col-md-4 " ><button type="submit" class="btn btn-success btn-block">Ubah</button></div>
					<div class="col-md-4 " ><button type="reset" onClick="history.go(0)" class="btn btn-danger btn-block">Reset</button></div>
						<div class="col-md-2 " ></div>
					
					</div>


				
				
			</div>
			</form>			
<link href="./css/start/jquery-ui-1.10.4.custom.css" rel="stylesheet">

<script src="./js/jquery-ui-1.10.4.custom.js"></script>
<script src="./js/jquery.price_format.2.0.min.js"></script>


<script src="js/bootstrap-datepicker.js"></script>
<link href="css/datepicker3.css" rel="stylesheet">					
</div>
	</div>
	<?php include('./static/pre-footer.php'); ?>
</div>


