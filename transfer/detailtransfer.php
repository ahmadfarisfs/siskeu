<?php
include(dirname(__FILE__).'/isboleh.php');
$uid=$_GET['id']; //mengambil id yang dikirim
$query="SELECT 
			*
		FROM transfer WHERE transferID=$uid";
$sss=mysql_query($query);
$row=mysql_fetch_array($sss);

?>
<script>
$(function() {
$('.nilai').priceFormat({
      prefix: 'Rp ',
     //centsSeparator: ',',
     allowNegative: 'true',
      centsLimit: 0,
	  thousandsSeparator: '.'
       });
 });
</script>
<div class="container" style="margin-top:90px">
	<div class="row">
		<?php include('./static/nav-left.php'); ?>
		<div class="col-md-10  ">
			<div class="panel panel-danger">
				<!-- Default panel contents -->
					<div class="panel-heading "><span class="glyphicon glyphicon-transfer"></span> <span class="glyphicon glyphicon-chevron-right"></span> Detail Transfer</div>
					<div class="panel-body text-right">
					<?php if(isbolehedit($row['createdBy'])){ ?>
				<div class="btn-group"><a class="btn btn-success " href="main.php?kon=edittransaction&id=<?php echo $row['transferID'];?>"><span class="glyphicon glyphicon-pencil"></span></a>
			<?php if(isbolehdelete($row['createdBy'])){?><a class="btn btn-danger" href="transfer/deletetransfer.php?id=<?php echo $row['transferID'];?>"
					   onclick="return confirm('Apakah Anda yakin?')" ><span class="glyphicon glyphicon-trash"></span></a><?php } ?>
		</div><?php  }  ?>
		</div>
<table class="table table-striped table-hover ">

	<tr>
		<th class="text-right"><b>ID</b></th>
		<td class="text-center"> : </td>
		<td><?php echo $row['transferID']; ?></td>
	</tr>

	<tr>
		<td class="text-right"><b>Nama Proyek Sumber</b></td>
		<td class="text-center"> : </td>
		<td><?php echo $row['srcprj']; ?> - <?php get_project_name( $row['srcprj']) ?></td>
	</tr>
	<tr>
		<td class="text-right"><b>Nama Proyek Tujuan</b></td>
		<td class="text-center"> : </td>
		<td><?php echo $row['dstaprj']; ?> - <?php get_project_name( $row['dstaprj']) ?></td>
	</tr>
	<tr>
		<td class="text-right"><b>Akun Tujuan</b></td>
		<td class="text-center"> : </td>
		<td class=""><?php echo $row['dstacc']; ?></td>
	</tr>
	<tr>
		<td class="text-right"><b>Nilai </b></td>
		<td class="text-center"> : </td>
		<td class="nilai"><?php echo $row['nilai']; ?></td>
	</tr>

	<tr>
		<td class="text-right"><b>Bukti </b></td>
		<td class="text-center"> : </td>
		<td class=""><?php echo $row['bukti']; ?></td>
	</tr>
	<tr>
		<td class="text-right"><b>Deskripsi</b></td>
		<td class="text-center"> : </td>
		<td><?php echo $row['deskripsi']; ?></td>
	</tr>
	<tr>
		<td class="text-right"><b>Tanggal</b></td>
		<td class="text-center"> : </td>
		<td><?php echo $row['tanggal']; ?></td>
	</tr>

	<tr>
		<td class="text-right"><b>Approval</b></td>
		<td class="text-center"> : </td>
		<td><?php styleryesno($row['approval']); ?></td>
	</tr>

	<tr>
		<td class="text-right"><b>Dibuat Pada</b></td>
		<td class="text-center"> : </td>
		<td><?php echo $row['createdOn']; ?></td>
	</tr>
	<tr>
		<td class="text-right"><b>Dibuat Oleh</b></td>
		<td class="text-center"> : </td>
		<td><?php echo $row['createdBy']; ?></td>
	</tr>
	<tr>
		<td class="text-right"><b>Dirubah Pada</b></td>
		<td class="text-center"> : </td>
		<td><?php echo $row['changedOn']; ?></td>
	</tr>
	

</table>



</div>
	</div>
	<?php include('./static/pre-footer.php'); ?>
</div>
