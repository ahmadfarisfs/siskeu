<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Aplikasi Keuangan LMBSP</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <nav class="navbar navbar-default navbar-fixed-top"  role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
		

      <a class="navbar-brand" href="#">Aplikasi Keuangan LMBSP</a>
 
  </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
     
      
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<div class="container" style="margin-top:90px">

	<div class="col-md-4 col-md-offset-4">
	<div class="panel panel-default">
	  <div class="panel-heading"><h3 class="panel-title"><strong>Sign in </strong></h3></div>
	  <div class="panel-body">
	   <form role="form">
	  <div class="form-group">
	  <label for="pengguna">Username</label>
		<div class="input-group input-group-lg">
			<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
			<input type="email" class="form-control" style="border-radius:0px" id="pengguna" placeholder="Enter username">
		</div>
	  	</div>
	  <div class="form-group">
	  <label for="katasandi">Password <a href="/user/changepass.php">(ganti password)</a></label>
	  <div class="input-group input-group-lg">
		<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
		<input type="password" class="form-control" style="border-radius:0px" id="katasandi" placeholder="Password">
	  </div>
	  </div>
	  <button type="submit" class="btn btn-success btn-block">Sign in</button>
	  <button type="" class="btn btn-info btn-block">Lupa Password ?</button>
	</form>
	  </div>
	</div>
	</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  	<script src="./js/jquery-1.10.2.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>