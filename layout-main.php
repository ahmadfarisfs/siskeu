<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Aplikasi Keuangan Laboratorium</title>
<!-- Bootstrap -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<a class="navbar-brand" href="#">Aplikasi Keuangan Laboratorium</a>
	</div>
	<ul class="nav navbar-nav navbar-right">
		<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown">User - PIP <b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="#"><span class="glyphicon glyphicon-pencil"></span> Edit</a></li>
			<li class="divider"></li>
			<li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		</ul>
		</li>
	</ul>
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	</div>
	<!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>
<div class="container" style="margin-top:90px">
	<div class="row">
		<div class="col-md-2">
			<ul class="nav nav-pills nav-stacked">
				<li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span> Home</a></li>
				<li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-user"></span> Pengguna</a></li>
				<li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-road"></span> Proyek</a></li>
				<li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-briefcase"></span> Transaksi</a></li>
				<li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-transfer"></span> Transfer</a></li>
				<li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-list-alt"></span> Laporan</a></li>
				<li><a href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-cloud-upload"></span> Unggah</a></li>
			</ul>
		</div>
		<div class="col-md-10">
			<div class="col-md-6">
				<div class="alert alert-dismissable alert-warning">
					<button type="button" class="close" data-dismiss="alert">X</button>
					<span class="glyphicon glyphicon-briefcase"></span> Ada <a href="#" class="alert-link">7 transaksi</a> yang membutuhkan approval anda.
				</div>
			</div>
			<div class="col-md-6">
				<div class="alert alert-dismissable alert-info">
					<button type="button" class="close" data-dismiss="alert">X</button>
					<span class="glyphicon glyphicon-transfer"></span> Ada <a href="#" class="alert-link">7 transfer</a> yang membutuhkan approval anda.
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<div class="jumbotron">
						<h1>Selamat Datang User Fullname !</h1>
						<p>
							10 transfer telah di approve, 300 transaksi telah di approve, 10 proyek sedang berjalan dan 10 user terdaftar.
						</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="./js/jquery-1.10.2.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>