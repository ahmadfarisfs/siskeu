<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Keuangan LMBSP-ITB</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <nav class="navbar navbar-default navbar-fixed-top"  role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
		

      <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-th-large"></span> Keuangan LMBSP-ITB</a>
 
	</div>
      <ul class="nav navbar-nav navbar-right">
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">User - PIP <b class="caret"></b></a>
          <ul class="dropdown-menu">
            <li><a href="#"><span class="glyphicon glyphicon-cog"></span> Edit</a></li>
            <li class="divider"></li>
            <li><a href="#"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
          </ul>
        </li>
      </ul>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      
    </div><!-- /.navbar-collapse -->
	
  </div><!-- /.container-fluid -->
</nav>
<div class="container" style="margin-top:90px">

<div class="row">
        <div class="col-md-2">
            <div class="list-group">
                <a class="list-group-item active" href="#"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                <a class="list-group-item" href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-user"></span> Pengguna</a>
                <a class="list-group-item" href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-road"></span> Proyek</a>
                <a class="list-group-item" href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-briefcase"></span> Transaksi</a>
                <a class="list-group-item" href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-transfer"></span> Transfer</a>
                <a class="list-group-item" href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-list-alt"></span> Laporan</a>
                <a class="list-group-item" href="http://www.jquery2dotnet.com"><span class="glyphicon glyphicon-cloud-upload"></span> Unggah</a>

            </div>
        </div>
					<div class="col-md-10">
					
					
			<div class="panel panel-default">
  <!-- Default panel contents -->
  <div class="panel-heading"><h3>List Proyek</h3></div>
  <div class="panel-body">
    <p><button type="button" class="btn btn-info pull-right"><span class="glyphicon glyphicon-plus"></span> Tambah Proyek Baru</button></p>
	<p><p>
  </div>		
  <table class="table table-striped table-hover ">
  <thead>
    <tr class="warning">
      <th >#</th>
      <th>ID</th>
      <th>Nama</th>
      <th>PIC</th>
      <th>Lokasi</th>
      <th>Nilai</th>
      <th>Tanggal</th>
      <th>Status</th>
      <th><span class="glyphicon glyphicon-wrench"></span></th>

    
	</tr>
  </thead>
  <tbody>
    <tr>
		<td>1</td>
        <td>48</td>
        <td><a href="main.php?kon=detailproject&id=48">Line Tupperware</a></td>
        <td>arief</td>
        <td>Kalimantan Tengah</td>
        <td class="nilai">90000000000</td>

		<td>2014-04-30</td>
        <td>Open</td>

		<td>				<a href="main.php?kon=editproject&id=48"><span class="glyphicon glyphicon-pencil"></span></a>
			<a href="project/deleteproject.php?id=48"
					   onclick="return confirm('Apakah Anda yakin?')" ><span class="glyphicon glyphicon-trash"></span></a>		</td>
    </tr>
    <tr>
		<td>2</td>
        <td>49</td>
        <td><a href="main.php?kon=detailproject&id=49">Masbro</a></td>
        <td>farissss</td>
        <td>sdas</td>
        <td class="nilai">900000</td>

		<td>2014-04-16</td>
        <td>Open</td>

		<td></td>
    </tr>
    <tr>
		<td>3</td>
        <td>50</td>
        <td><a href="main.php?kon=detailproject&id=50">dasa</a></td>
        <td>farissss</td>
        <td>fdsf</td>
        <td class="nilai">70000000</td>

		<td>0000-00-00</td>
        <td>Open</td>

		<td></td>
    </tr>
    <tr>
		<td>4</td>
        <td>57</td>
        <td><a href="main.php?kon=detailproject&id=57">dibikin faris</a></td>
        <td>arief</td>
        <td>kokop</td>
        <td class="nilai">900000</td>

		<td>0000-00-00</td>
        <td>Open</td>

		<td>			
					</td>
    </tr>
    <tr>
		<td>5</td>
        <td>58</td>
        <td><a href="main.php?kon=detailproject&id=58">Pabrik Prolink</a></td>
        <td>faris</td>
        <td>Bantuk</td>
        <td class="nilai">5000000000</td>

		<td>2014-04-18</td>
        <td>Open</td>
        
		<td></td>
    </tr>

  </tbody>
</table> </div>
					</div>
				</div>

	<div class="row">
		<div class="well well-sm col-md-12 text-center">
			(c) LMBSP 2014
		</div>
	</div>
	</div>
	</div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  	<script src="./js/jquery-1.10.2.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>