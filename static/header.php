<!DOCTYPE html>
<?php include(dirname(__FILE__).'/genfu.php'); ?>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Keuangan LMBSP-ITB</title>
<!-- Bootstrap -->
<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/jasny-bootstrap.min.css" rel="stylesheet" media="screen">
<link href="./css/footable.core.css" rel="stylesheet" type="text/css" />
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
      <script src="html5shiv.js"></script>
      <script src="respond.min.js"></script>
    <![endif]-->
<script src="js/jquery-1.10.2.js"></script>
<script src="./js/jquery.price_format.2.0.min.js"></script>
<script src="./js/footable.js" type="text/javascript"></script>
<script src="./js/footable.sort.js" type="text/javascript"></script>

</head>
<body>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
<div class="container-fluid">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
	
		<a class="navbar-brand" href="main.php?kon=home"><img src="./img/logo.png"> Project Management System</a>
	</div>
	<ul class="nav navbar-nav navbar-right">
		<li class="dropdown">
		<a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $_SESSION['username']; ?> - <?php echo $nama['grupid']; ?><b class="caret"></b></a>
		<ul class="dropdown-menu">
			<li><a href="main.php?kon=edituser&id=<?php echo $_SESSION['user_id'];?>"><span class="glyphicon glyphicon-cog"></span> Edit</a></li>
			<li class="divider"></li>
			<li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
		</ul>
		</li>
	</ul>
	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	</div>
	<!-- /.navbar-collapse -->
</div>
<!-- /.container-fluid -->
</nav>