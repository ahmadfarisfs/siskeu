<?php include('static/header-nouser.php'); ?><script src="./js/jquery-1.10.2.js"></script>

<div class="container" style="margin-top:90px">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-warning">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>Reset Password </strong></h3>
			</div>
			<div class="panel-body">
				Password baru akan dikirimkan melalui email menuju alamat email anda
				<form role="form" method="post" action="forget.php" AUTOCOMPLETE="off">
					<div class="form-group">
						<label for="exampleInputEmail1">Username</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
							<input name="pengguna" type="text" class="form-control" style="border-radius:0px" id="exampleInputEmail1" placeholder="Enter username">
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">E-mail</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon"><span class="glyphicon glyphicon-file"></span></span>
							<input name="email" type="email" class="form-control" style="border-radius:0px" id="exampleInputPassword1" placeholder="E-mail">
						</div>
					</div>
					<button type="submit" class="btn btn-success btn-block">RESET PASSWORD</button>
				</form>
			</div>
		</div>
	</div>
<?php include('static/pre-footer.php'); ?>
</div>
<?php include('static/footer.php'); ?>
