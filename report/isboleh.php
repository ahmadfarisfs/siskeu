<?php

if (!($_SESSION['grupid']=='PIP' or $_SESSION['grupid']=='MGT' or $_SESSION['grupid']=='BDH'))
{
	//header('Location: main.php?kon=err&id=1');
?>
	<script type="text/javascript">window.location = "main.php?kon=err&id=1"</script><?php
	die();
}

function isboleh()
{
	if ($_SESSION['grupid']=='PIP' or $_SESSION['grupid']=='MGT' or $_SESSION['grupid']=='BDH' )
	{
		return 1;
	}
	else {return 0;}
}
function isbolehtambah()
{
	if ($_SESSION['grupid']=='PIP' )
	{
		return 1;
	}
	else {return 0;}
}

function isbolehedit($createdby,$pipro)
{
	if ($_SESSION['grupid']=='PIP' && $_SESSION['username']==$createdby or $pipro==$_SESSION['username'])
	{
		return 1;
	}
	else {return 0;}
}

function isbolehdelete($createdby)
{
	if ($_SESSION['grupid']=='PIP' && $_SESSION['username']==$createdby  )
	{
		return 1;
	}
	else {return 0;}
}
?>