<?php include('static/header-nouser.php'); ?><script src="./js/jquery-1.10.2.js"></script>

<div class="container" style="margin-top:90px">
	<div class="col-md-4 col-md-offset-4">
		<div class="panel panel-warning">
			<div class="panel-heading">
				<h3 class="panel-title"><strong>Sign in </strong></h3>
			</div>
			<div class="panel-body">
				<form role="form" method="post" action="proseslogin.php" AUTOCOMPLETE="off">
					<div class="form-group">
						<label for="exampleInputEmail1">Username</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
							<input name="pengguna" type="text" class="form-control" style="border-radius:0px" id="exampleInputEmail1" placeholder="Enter username">
						</div>
					</div>
					<div class="form-group">
						<label for="exampleInputPassword1">Password</label>
						<div class="input-group input-group-lg">
							<span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
							<input name="katasandi" type="password" class="form-control" style="border-radius:0px" id="exampleInputPassword1" placeholder="Password">
						</div>
					</div>
					<button type="submit" class="btn btn-success btn-block">Sign in</button>
					<a href="formforget.php"  class="btn btn-info btn-block">Lupa Password ?</a>
				</form>
			</div>
		</div>
	</div>
<?php include('static/pre-footer.php'); ?>
</div>
<?php include('static/footer.php'); ?>

